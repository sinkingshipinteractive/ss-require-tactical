/* global require, module */
'use strict';

	// other plugins
var fs = require('fs');
var through = require('through');
var path = require('path');

var stripJsonComments = require('strip-json-comments');

var gutil = require('gulp-util');
var File = gutil.File;
var Buffer = require('buffer').Buffer;

	// local instance for including files
var requireTactical;
var output;

// Exporting the plugin main function
module.exports = function( file, opt, type ) {

	if (type === 'SpringRoll') {
		var rt = new RequireTactical(opt);
		rt.includeURI(file);
		return rt;
	}

	output = file;

		// initialize require
	requireTactical =  new RequireTactical ( opt );

  	return through ( bufferContents, endStream );
};

function bufferContents ( file ) {

	if ( typeof file === 'string' ) {

  					// start require with a URI
		requireTactical.includeURI ( file );
		throw 'is string';
	} else if ( typeof file.path === 'string' ) {

		requireTactical.includeFile ( file );
  	}
 }

function endStream ( ) {
		// create a new file that will be piped, most likely moved to abuild folder
	var file = new File ( { 'path' : output, 'contents' : new Buffer ( requireTactical.compile () ) } );
	this.emit ( 'data', file ); // jshint ignore:line
    this.emit ( 'end' ); // jshint ignore:line
}

function RequireTactical ( config ) {

	var _this = this;
	var _requiredScriptSet;
	var _isLoadingSet;
	var _config;
	var _buffer; // jshint ignore:line
	var _loadCount = 0; // jshint ignore:line

	function _construct ( config ) {

			// set up a default config in case we don;t have one
		config = config || {};

		_requiredScriptSet = [];
		_isLoadingSet = [];
		_config = { 'baseUrl' : config.baseURL || '', 'paths' : {}, 'alias' : {}, 'debug' : false, 'console' : false };

		return _this;
	}

	function _isEmpty ( value ) {

		return ( typeof ( value ) === String ( undefined ) || value === null );
	}

	function _findBaseName ( uri ) {
	    var fileName = uri.substring ( uri.lastIndexOf ( '/' ) + 1 );
	    var dot = fileName.lastIndexOf ( '.' );
	    return dot == -1 ? fileName : fileName.substring ( 0, dot );
	}

	function _stripJSExtension ( uri ) {
		var ext = uri.substr(-3, 3);
		if (ext === '.js') {
			uri = uri.substring(0, uri.length - 3);
		}
		return uri;
	}

	function _isScriptLoaded ( scriptName ) {

		var scriptFound = false;
		for( var i = 0; i < _requiredScriptSet.length; i++ ) {
			if ( _stripJSExtension ( scriptName ) == _stripJSExtension ( _requiredScriptSet [ i ].name ) ) {
				scriptFound = true;
			}
		}
			// this help prevent curcular dependecies... Well somewhat
		if ( _isLoadingSet.indexOf ( _stripJSExtension ( scriptName ) ) > -1 ) {
			scriptFound = true;
		}

		return scriptFound;
	}

	function _getBase () {
		var baseUrl = _config.baseUrl;
		return ( baseUrl.substr ( -1 ) === '/' || baseUrl === '' ) ? baseUrl : baseUrl + '/';
	}

	function _getAliases ( scriptName ) {

		var alias = _config.alias [ scriptName ];

			// there's no alias so send back the script's url
		if ( scriptName instanceof Array ) {
			alias = scriptName;
		} else if ( _isEmpty ( alias ) ) {
			alias = [ scriptName ];
		} else if ( typeof ( alias ) == 'string' ) {
			alias = [ alias ];
		}

		return alias;
	}

	function _getScripts ( scriptName ) {

			// extract an alias if one is available
		var scriptSet = _getAliases ( scriptName );

			// let get magical. If a array is passed in we import the array
		for( var i = 0; i < scriptSet.length; i++ ) {
			scriptSet [ i ] = ( scriptSet [ i ].match ( /(.+?\/)*.+?\.js/ ) ) ? scriptSet [ i ] : scriptSet [ i ] + '.js';
		}

		return scriptSet;
	}

	function _importDependencies ( response ) {
			// find any import and import those files
		var preprocessRegExp = /(require.import|require.config|require.include)\s*\(([\S\s]+?)\)\s*\;?/gi;
		var result;

		while( !_isEmpty ( result = preprocessRegExp.exec ( response ) ) ) {

			switch( result [ 1 ] ) {
				case 'require.config' :
					_setConfig ( JSON.parse ( stripJsonComments ( result [ 2 ] ) ) );
					// console.log ( _config );
					break;
				default :

					var param =  eval ( stripJsonComments ( result [ 2 ] ) ); // jshint ignore:line

					if ( param instanceof Array ) {
						for( var i = 0; i < param.length; i++ ) {
							_importURI ( param [ i ] );
						}
					} else {
						_importURI ( param );
					}

			}
		}

	}

	function _processScriptResponse ( file, scriptName, scriptURL ) {

			// first import any dependencies. Note this function is recursive
		_importDependencies ( file.toString ().replace(/\/\*.+?\*\/|\/\/.*(?=[\n\r])/g, '') );

			// now add any processing, mostly debug stuff
		return { 'file' : file, 'name' : scriptName, 'uri' : scriptURL };
	}

	function _importURI ( scriptName ) {

		// console.log ( _isLoadingSet.indexOf ( scriptName ) );

			// basic error tracking and checking if the file isw loaded
		if ( _isScriptLoaded ( scriptName ) || _isEmpty ( scriptName ) ) {
			return;
		}

			// prevent circular dependencies
		_isLoadingSet.push ( _stripJSExtension ( scriptName ) );

		var basePath = _getBase ();
		var scriptURISet = _getScripts ( scriptName );

		for( var i = 0; i < scriptURISet.length; i++ ) {
			var scriptURI = basePath + scriptURISet [ i ];
			var file;
			try {
					// load the file synchronously
				file = fs.readFileSync ( scriptURI ); // jshint ignore:line
			} catch ( e ) {
				// e.code
				throw e;
			}

				// mark the script as loaded so we don't load again later
				// the scripts are store as the file, name, uri
	  		_requiredScriptSet.push ( _processScriptResponse ( file, scriptName, scriptURI ) );

	  			// done loading the script we can remove it
	  		// _isLoadingSet.splice ( _isLoadingSet.indexOf ( scriptName ), 1 );
		}
	}

	function _setConfig ( config ) {
			// just loop through and overwrite properties
		for( var property in config ) {
				// filter out these properties
			if ( property != 'baseUrl' ) {
				_config [ property ] = config [ property ];
			}
		}

		if ( _config.include instanceof Array ) {
			for( var i = 0; i < _config.include.length; i++ ) {
				_importURI ( _config.include[ i ] );
			}
		}
	}

	function _getFileContent ( file ) {
		return ( file.contents ) ? file.contents : file.toString ();
	}

	_this.includeURI = function( uri ) {
		_importURI ( uri );
	};

	_this.includeFile = function( file ) {

			// get the scriptURI. I could probably remove this
		var scriptURI = path.basename ( file.path );

			// first import any dependencies. Note this function is recursive
		_importDependencies ( _getFileContent ( file ) );

			// now add any processing, mostly debug stuff
		_requiredScriptSet.push ( { 'file' : file, 'name' : _findBaseName ( scriptURI ), 'uri' : scriptURI } );
	};

	_this.compile = function() {

		var output = 'var require={"include":function(){},"config":function(){},"ready":function(e){ $( document ).ready ( e ) }};';
		// output = '(function(e,t){function s(){if(!r){r=true;for(var e=0;e<n.length;e++){n[e].fn.call(window,n[e].ctx)}n=[]}}function o(){if(document.readyState==="complete"){s()}}e=e||"docReady";t=t||window;var n=[];var r=false;var i=false;t[e]=function(e,t){if(r){setTimeout(function(){e(t)},1);return}else{n.push({fn:e,ctx:t})}if(document.readyState==="complete"){setTimeout(s,1)}else if(!i){if(document.addEventListener){document.addEventListener("DOMContentLoaded",s,false);window.addEventListener("load",s,false)}else{document.attachEvent("onreadystatechange",o);window.attachEvent("onload",s)}i=true}}})("docReady",window);var require={include:function(){},config:function(){},ready:function(e){docReady(e)}};';
		for( var i = 0; i < _requiredScriptSet.length; i++ ) {
			var file = _requiredScriptSet [ i ].file;
			output += _getFileContent ( file ) + '\n;';
		}

		// output = output.replace ( /(require.import|require.config|require.include)\s*\(([\S\s]+?)\)\s*\;?/gi, '' );

			// also filter out console
		return output; // ( _config [ 'console' ] === false ) ? deconsole ( output ) : output;
	};

	_this.doInject = function ( code ) {
		var injectRegExp = /(require.inject)\s*\(([\S\s]+?)\)\s*\;?/gi;
		var result;

		while ( !_isEmpty ( result = injectRegExp.exec ( code ) ) ) { 

				// with injection there should only be one
			var scriptURL = _getScripts ( eval ( result [ 2 ] ) ); // jshint ignore:line
			var fileInject = fs.readFileSync ( _getBase () + scriptURL [ 0 ] , 'utf8' );
			code = code.replace ( result [ 0 ], fileInject );
		}

		return code;
		
	};

	_this.getScriptListsForSpringroll = function() {
		var librariesOutput = '';
		var mainOutput = '';
		for( var i = 0; i < _requiredScriptSet.length - 1; i++ ) {
			var scriptname = _requiredScriptSet [ i ].name;
			var scriptLine;
			
			if (scriptname.indexOf('.js') == -1) {
				 scriptLine = '\t\t"' + 'src/' + _requiredScriptSet [ i ].name + '.js",\n';
			} else {
				scriptLine = '\t\t"' + 'src/' + _requiredScriptSet [ i ].name + '",\n';
			}

			if(scriptLine.indexOf('weblib') !== -1) {
				librariesOutput += scriptLine;			
			} else {
				mainOutput += scriptLine;
			}

		}

		librariesOutput = librariesOutput.substr(2, librariesOutput.length - 4);
		mainOutput = mainOutput.substr(2, mainOutput.length - 4);
		return [mainOutput, librariesOutput];
	};


	return _construct ( config );
}
